﻿#region MIT License
/*
 * Copyright (c) 2024 Glenn Alon
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
#endregion

using System.Collections.Generic;

namespace GradeCalculator.Winforms
{
    internal class CourseSubjectClass
    {
        private List<string> subjects = new List<string>();
        public List<string> BSCS(int level, int term)
        {
            switch (level)
            {
                case 0 when term == 0:
                    subjects.Add("Introduction to Computing");
                    subjects.Add("Computer Programming 1");
                    subjects.Add("The Contemporary World");
                    subjects.Add("Euthenics 1");
                    subjects.Add("Mathematics in the Modern World");
                    subjects.Add("National Service Training Program 1");
                    subjects.Add("P.E/.PATHFIT 1: Movement Competency Training");
                    subjects.Add("Understanding the Self");
                    break;
                case 0 when term == 1:
                    subjects.Add("Computer Programming 2");
                    subjects.Add("Discrete Structures 1 (Discrete Mathematics)");
                    subjects.Add("Art Appreciation");
                    subjects.Add("National Service Training Program 2");
                    subjects.Add("P.E./PATHFIT 2: Exercise-based Fitness Activities");
                    subjects.Add("Purposive Communication");
                    subjects.Add("Science, Technology, and Society");
                    subjects.Add("College Calculus");
                    break;
                case 1 when term == 0:
                    subjects.Add("Data Structures and Algorithms");
                    subjects.Add("Discrete Structures 2");
                    subjects.Add("The Entrepreneurial Mind");
                    subjects.Add("Rizal's Life and Works");
                    subjects.Add("P.E./PATHFIT 3: Individual-Dual Sports");
                    subjects.Add("Readings in Philippine History");
                    subjects.Add("Principles of Communication");
                    subjects.Add("Computer Programming 3");
                    break;
                case 1 when term == 1:
                    subjects.Add("Design and Analysis of Algorithms");
                    subjects.Add("Information Management");
                    subjects.Add("Philippine Popular Culture");
                    subjects.Add("Ethics");
                    subjects.Add("P.E./PATHFIT 4: Team Sports");
                    subjects.Add("Human-Computer Interaction");
                    subjects.Add("Great Books");
                    subjects.Add("CS Elective 1");
                    subjects.Add("Computer Systems Architecture");
                    break;
                case 2 when term == 0:
                    subjects.Add("Theory of Computations with Automata");
                    subjects.Add("Quantitative Methods (Data Analysis)");
                    subjects.Add("Information Assurance & Security (Cybersecurity Fundamentals)");
                    subjects.Add("CS Elective 2");
                    subjects.Add("Artificial Intelligence");
                    subjects.Add("Application Development and Emerging Technologies");
                    subjects.Add("Software Engineering 1");
                    subjects.Add("Methods of Research");
                    break;
                case 2 when term == 1:
                    subjects.Add("Modeling and Simulation");
                    subjects.Add("Computer Organization");
                    subjects.Add("Software Engineering 2");
                    subjects.Add("Game Programming");
                    subjects.Add("Programming Languages");
                    subjects.Add("CS Elective 3");
                    subjects.Add("CS Thesis 1");
                    subjects.Add("Methods of Research");
                    break;
                case 3 when term == 0:
                    subjects.Add("Network Technology 1");
                    subjects.Add("Platform Technology (Operating Systems)");
                    subjects.Add("CS Thesis 2");
                    subjects.Add("Euthenics 2");
                    subjects.Add("Technopreneurship");
                    subjects.Add("Professional Issues in Information Systems and Technology");
                    subjects.Add("Information Assurance and Security (Data Privacy)");
                    subjects.Add("Software Quality Assurance");
                    break;
                case 3 when term == 1:
                    subjects.Add("CS Practicum (300 hours)");
                    break;
            }
            return subjects;
        }
        public List<string> BSIT(int level, int term)
        {
            switch (level)
            {
                case 0 when term == 0:
                    subjects.Add("Introduction to Computing");
                    subjects.Add("Computer Programming 1");
                    subjects.Add("The Contemporary World");
                    subjects.Add("Euthenics 1");
                    subjects.Add("Mathematics in the Modern World");
                    subjects.Add("National Service Training Program 1");
                    subjects.Add("P.E/.PATHFIT 1: Movement Competency Training");
                    subjects.Add("The Entrepreneurial Mind");
                    subjects.Add("Understanding the Self");
                    break;
                case 0 when term == 1:
                    subjects.Add("Computer Programming 2");
                    subjects.Add("Discrete Structures 1 (Discrete Mathematics)");
                    subjects.Add("Art Appreciation");
                    subjects.Add("National Service Training Program 2");
                    subjects.Add("P.E./PATHFIT 2: Exercise-based Fitness Activities");
                    subjects.Add("Purposive Communication");
                    subjects.Add("Science, Technology, and Society");
                    subjects.Add("Ethics");
                    subjects.Add("Systems Administration and Maintenance");
                    break;
                case 1 when term == 0:
                    subjects.Add("Data Structures and Algorithms");
                    subjects.Add("Readings in Philippine History");
                    subjects.Add("P.E./PATHFIT 3: Individual-Dual Sports");
                    subjects.Add("Rizal’s Life and Works");
                    subjects.Add("Human-Computer Interaction");
                    subjects.Add("IT Elective 1");
                    subjects.Add("Principles of Communication");
                    subjects.Add("Platform Technology (Operating Systems)");
                    break;
                case 1 when term == 1:
                    subjects.Add("Information Management");
                    subjects.Add("Philippine Popular Culture");
                    subjects.Add("P.E./PATHFIT 4: Team Sports");
                    subjects.Add("Network Technology 1");
                    subjects.Add("Quantitative Methods");
                    subjects.Add("Technopreneurship");
                    subjects.Add("Systems Integration and Architecture");
                    subjects.Add("Integrative Programming");
                    break;
                case 2 when term == 0:
                    subjects.Add("Application Development and Emerging Technologies");
                    subjects.Add("Advanced Database Systems");
                    subjects.Add("Event-Driven Programming");
                    subjects.Add("Data and Digital Communications (Data Communications)");
                    subjects.Add("Professional Issues in Information Systems and Technology");
                    subjects.Add("IT Elective 2");
                    subjects.Add("Advanced Systems Integration and Architecture");
                    break;
                case 2 when term == 1:
                    subjects.Add("Web Systems and Technologies");
                    subjects.Add("Management Information Systems");
                    subjects.Add("IT Capstone Project 1");
                    subjects.Add("Great Books");
                    subjects.Add("IT Elective 3");
                    subjects.Add("Mobile Systems and Technologies");
                    subjects.Add("Information Assurance and Security (Cybersecurity Fundamentals)");
                    break;
                case 3 when term == 0:
                    subjects.Add("Euthenics 2");
                    subjects.Add("IT Capstone Project 2");
                    subjects.Add("IT Elective 4");
                    subjects.Add("Computer Graphics Programming");
                    subjects.Add("IT Service Management");
                    subjects.Add("Information Assurance and Security (Data Privacy)");
                    subjects.Add("Network Technology 2");
                    break;
                case 3 when term == 1:
                    subjects.Add("IT Practicum (486 hours)");
                    break;
            }
            return subjects;
        }
    }
}
