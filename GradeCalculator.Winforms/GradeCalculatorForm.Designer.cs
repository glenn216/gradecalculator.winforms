﻿#region MIT License
/*
 * Copyright (c) 2024 Glenn Alon
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
#endregion

using System.Drawing;
using System.Windows.Forms;

namespace GradeCalculator.Winforms
{
    partial class GradeCalculatorForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            groupBox2 = new GroupBox();
            submitBtn = new Button();
            statusLbl = new Label();
            button2 = new Button();
            button1 = new Button();
            subjectsCb = new ComboBox();
            label8 = new Label();
            finalsTb = new TextBox();
            prefinalsTb = new TextBox();
            midtermsTb = new TextBox();
            prelimsTb = new TextBox();
            label7 = new Label();
            label6 = new Label();
            label5 = new Label();
            label4 = new Label();
            groupBox3 = new GroupBox();
            outputLbl = new Label();
            dataGridView1 = new DataGridView();
            Subject = new DataGridViewTextBoxColumn();
            Prelims = new DataGridViewTextBoxColumn();
            Midterms = new DataGridViewTextBoxColumn();
            Prefinals = new DataGridViewTextBoxColumn();
            Finals = new DataGridViewTextBoxColumn();
            FinalGrade = new DataGridViewTextBoxColumn();
            groupBox2.SuspendLayout();
            groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(submitBtn);
            groupBox2.Controls.Add(statusLbl);
            groupBox2.Controls.Add(button2);
            groupBox2.Controls.Add(button1);
            groupBox2.Controls.Add(subjectsCb);
            groupBox2.Controls.Add(label8);
            groupBox2.Controls.Add(finalsTb);
            groupBox2.Controls.Add(prefinalsTb);
            groupBox2.Controls.Add(midtermsTb);
            groupBox2.Controls.Add(prelimsTb);
            groupBox2.Controls.Add(label7);
            groupBox2.Controls.Add(label6);
            groupBox2.Controls.Add(label5);
            groupBox2.Controls.Add(label4);
            groupBox2.FlatStyle = FlatStyle.Flat;
            groupBox2.Font = new Font("Segoe UI", 9F);
            groupBox2.Location = new Point(12, 12);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(410, 197);
            groupBox2.TabIndex = 7;
            groupBox2.TabStop = false;
            groupBox2.Text = "Subject Grades";
            // 
            // submitBtn
            // 
            submitBtn.BackColor = Color.SpringGreen;
            submitBtn.FlatStyle = FlatStyle.Flat;
            submitBtn.Location = new Point(312, 138);
            submitBtn.Name = "submitBtn";
            submitBtn.Size = new Size(87, 43);
            submitBtn.TabIndex = 21;
            submitBtn.Text = "Submit";
            submitBtn.UseVisualStyleBackColor = false;
            submitBtn.Click += submitBtn_Click;
            // 
            // statusLbl
            // 
            statusLbl.Location = new Point(196, 57);
            statusLbl.Name = "statusLbl";
            statusLbl.Size = new Size(117, 23);
            statusLbl.TabIndex = 19;
            statusLbl.TextAlign = ContentAlignment.TopCenter;
            // 
            // button2
            // 
            button2.FlatStyle = FlatStyle.Flat;
            button2.Location = new Point(319, 55);
            button2.Name = "button2";
            button2.Size = new Size(75, 23);
            button2.TabIndex = 18;
            button2.Text = "Forward";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click;
            // 
            // button1
            // 
            button1.FlatStyle = FlatStyle.Flat;
            button1.Location = new Point(115, 55);
            button1.Name = "button1";
            button1.Size = new Size(75, 23);
            button1.TabIndex = 17;
            button1.Text = "Back";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // subjectsCb
            // 
            subjectsCb.DropDownStyle = ComboBoxStyle.DropDownList;
            subjectsCb.FlatStyle = FlatStyle.Flat;
            subjectsCb.FormattingEnabled = true;
            subjectsCb.Location = new Point(116, 28);
            subjectsCb.Name = "subjectsCb";
            subjectsCb.Size = new Size(278, 23);
            subjectsCb.TabIndex = 16;
            subjectsCb.SelectedIndexChanged += subjectsCb_SelectedIndexChanged;
            // 
            // label8
            // 
            label8.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label8.Location = new Point(27, 28);
            label8.Name = "label8";
            label8.Size = new Size(70, 23);
            label8.TabIndex = 15;
            label8.Text = "Subject:";
            // 
            // finalsTb
            // 
            finalsTb.Location = new Point(116, 158);
            finalsTb.Name = "finalsTb";
            finalsTb.Size = new Size(190, 23);
            finalsTb.TabIndex = 14;
            finalsTb.KeyPress += finalsTb_KeyPress;
            // 
            // prefinalsTb
            // 
            prefinalsTb.Location = new Point(116, 135);
            prefinalsTb.Name = "prefinalsTb";
            prefinalsTb.Size = new Size(190, 23);
            prefinalsTb.TabIndex = 13;
            prefinalsTb.KeyPress += prefinalsTb_KeyPress;
            // 
            // midtermsTb
            // 
            midtermsTb.Location = new Point(116, 112);
            midtermsTb.Name = "midtermsTb";
            midtermsTb.Size = new Size(190, 23);
            midtermsTb.TabIndex = 12;
            midtermsTb.KeyPress += midtermsTb_KeyPress;
            // 
            // prelimsTb
            // 
            prelimsTb.Location = new Point(116, 89);
            prelimsTb.Name = "prelimsTb";
            prelimsTb.Size = new Size(190, 23);
            prelimsTb.TabIndex = 11;
            prelimsTb.KeyPress += prelimsTb_KeyPress;
            // 
            // label7
            // 
            label7.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label7.Location = new Point(45, 158);
            label7.Name = "label7";
            label7.Size = new Size(65, 23);
            label7.TabIndex = 10;
            label7.Text = "Finals:";
            // 
            // label6
            // 
            label6.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label6.Location = new Point(17, 133);
            label6.Name = "label6";
            label6.Size = new Size(87, 23);
            label6.TabIndex = 9;
            label6.Text = "Pre-finals:";
            // 
            // label5
            // 
            label5.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label5.Location = new Point(17, 110);
            label5.Name = "label5";
            label5.Size = new Size(87, 23);
            label5.TabIndex = 8;
            label5.Text = "Midterms:";
            // 
            // label4
            // 
            label4.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label4.Location = new Point(30, 87);
            label4.Name = "label4";
            label4.Size = new Size(67, 23);
            label4.TabIndex = 7;
            label4.Text = "Prelims:";
            // 
            // groupBox3
            // 
            groupBox3.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left;
            groupBox3.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            groupBox3.Controls.Add(outputLbl);
            groupBox3.Controls.Add(dataGridView1);
            groupBox3.FlatStyle = FlatStyle.Flat;
            groupBox3.Location = new Point(12, 215);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(876, 245);
            groupBox3.TabIndex = 8;
            groupBox3.TabStop = false;
            groupBox3.Text = "Grades List";
            // 
            // outputLbl
            // 
            outputLbl.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Right;
            outputLbl.Location = new Point(645, 19);
            outputLbl.Name = "outputLbl";
            outputLbl.Size = new Size(216, 216);
            outputLbl.TabIndex = 9;
            // 
            // dataGridView1
            // 
            dataGridView1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Location = new Point(17, 22);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.Size = new Size(622, 217);
            dataGridView1.TabIndex = 0;
            // 
            // Subject
            // 
            Subject.HeaderText = "Subject";
            Subject.Name = "Subject";
            Subject.ReadOnly = true;
            // 
            // Prelims
            // 
            Prelims.HeaderText = "Prelims";
            Prelims.Name = "Prelims";
            Prelims.ReadOnly = true;
            // 
            // Midterms
            // 
            Midterms.HeaderText = "Midterms";
            Midterms.Name = "Midterms";
            Midterms.ReadOnly = true;
            // 
            // Prefinals
            // 
            Prefinals.HeaderText = "Prefinals";
            Prefinals.Name = "Prefinals";
            Prefinals.ReadOnly = true;
            // 
            // Finals
            // 
            Finals.HeaderText = "Finals";
            Finals.Name = "Finals";
            Finals.ReadOnly = true;
            // 
            // FinalGrade
            // 
            FinalGrade.HeaderText = "Final Grade";
            FinalGrade.Name = "FinalGrade";
            FinalGrade.ReadOnly = true;
            // 
            // GradeCalculatorForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(900, 465);
            Controls.Add(groupBox3);
            Controls.Add(groupBox2);
            Name = "GradeCalculatorForm";
            Text = "Grade Calculator Form";
            FormClosed += GradeCalculatorForm_FormClosed;
            Load += Form1_Load;
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
        }

        #endregion
        private GroupBox groupBox2;
        private Label label6;
        private Label label5;
        private Label label4;
        private TextBox finalsTb;
        private TextBox prefinalsTb;
        private TextBox midtermsTb;
        private TextBox prelimsTb;
        private Label label7;
        private GroupBox groupBox3;
        private DataGridView dataGridView1;
        private ComboBox subjectsCb;
        private Label label8;
        private Button button2;
        private Button button1;
        private Label statusLbl;
        private Button submitBtn;
        private DataGridViewTextBoxColumn Subject;
        private DataGridViewTextBoxColumn Prelims;
        private DataGridViewTextBoxColumn Midterms;
        private DataGridViewTextBoxColumn Prefinals;
        private DataGridViewTextBoxColumn Finals;
        private DataGridViewTextBoxColumn FinalGrade;
        private Label outputLbl;
    }
}
