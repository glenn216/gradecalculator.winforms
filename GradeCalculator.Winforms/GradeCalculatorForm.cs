#region MIT License
/*
 * Copyright (c) 2024 Glenn Alon
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GradeCalculator.Winforms.Exceptions;

namespace GradeCalculator.Winforms
{
    public partial class GradeCalculatorForm : Form
    {
        private string? name;
        private int program = -1, level = -1, term = -1;
        private List<string> subjects_list = new();
        private List<double> general_weighted_average = new();
        private int count;
        public GradeCalculatorForm(string _name, int _program, int _level, int _term)
        {
            InitializeComponent();
            name = _name;
            program = _program;
            level = _level;
            term = _term;
            outputLbl.Text = $"Name: {name}{Environment.NewLine}" +
                             $"General Weighted Average: 0.00"; 
        }
        public GradeCalculatorForm()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { Subject, Prelims, Midterms, Prefinals, Finals, FinalGrade });
            if (program == 0) //BSCS
            {
                subjects_list = new CourseSubjectClass().BSCS(level, term);
            }
            if (program == 1) //BSIT
            {
                subjects_list = new CourseSubjectClass().BSIT(level, term);
            }
            foreach (string subject in subjects_list)
            {
                subjectsCb.Items.Add(subject);
            }
            if (subjectsCb.Items.Count > 0)
            {
                subjectsCb.SelectedIndex = 0;
            }
        }
        private void GradeCalculatorForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(0);
        }
        private void CalculateAndDisplayOutput()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(prelimsTb.Text) || string.IsNullOrWhiteSpace(midtermsTb.Text) || string.IsNullOrWhiteSpace(prefinalsTb.Text) || string.IsNullOrWhiteSpace(finalsTb.Text))
                {
                    throw new EmptyFieldsException("ERROR: Required fields cannot be empty. Please try again!");
                }

                double PRELIMS = Convert.ToDouble(prelimsTb.Text);
                double MIDTERMS = Convert.ToDouble(midtermsTb.Text);
                double PREFINALS = Convert.ToDouble(prefinalsTb.Text);
                double FINALS = Convert.ToDouble(finalsTb.Text);
                double RESULT;
                if (PRELIMS > 100 || MIDTERMS > 100 || PREFINALS > 100 || FINALS > 100)
                {
                    throw new GradeInputOutOfBoundsException("ERROR: The grade cannot be greater than 100. Please try again!");
                }

                PRELIMS *= .20;
                MIDTERMS *= .20;
                PREFINALS *= .20;
                FINALS *= .40;

                RESULT = PRELIMS + MIDTERMS + PREFINALS + FINALS;
                RESULT = Math.Round(RESULT, 2);

                general_weighted_average.Add(gpScale(RESULT));

                dataGridView1.Rows.Add(subjectsCb.SelectedItem.ToString(), prelimsTb.Text, midtermsTb.Text, prefinalsTb.Text, finalsTb.Text, gpScale(RESULT).ToString());  
                outputLbl.Text = $"Name: {name}{Environment.NewLine}General Weighted Average: {Math.Round(general_weighted_average.Average(), 2)}";

                subjectsCb.SelectedIndex += 1;
            }
            catch (EmptyFieldsException) { }
            catch (GradeInputOutOfBoundsException) { }
            finally
            {
                Clear();
            }
        }
        private void Clear()
        {
            prelimsTb.Clear();
            midtermsTb.Clear();
            prefinalsTb.Clear();
            finalsTb.Clear();
        }
        private double gpScale(double GRADE)
        {
            if (GRADE >= 97.50 && GRADE <= 100)
            {
                return 1.00;
            }
            else if (GRADE >= 94.50 && GRADE <= 97.49)
            {
                return 1.25;
            }
            else if (GRADE >= 91.50 && GRADE <= 94.49)
            {
                return 1.50;
            }
            else if (GRADE >= 88.50 && GRADE <= 91.49)
            {
                return 1.75;
            }
            else if (GRADE >= 85.50 && GRADE <= 88.49)
            {
                return 2.00;
            }
            else if (GRADE >= 82.50 && GRADE <= 85.49)
            {
                return 2.25;
            }
            else if (GRADE >= 79.50 && GRADE <= 82.49)
            {
                return 2.50;
            }
            else if (GRADE >= 76.50 && GRADE <= 79.49)
            {
                return 2.75;
            }
            else if (GRADE >= 74.50 && GRADE <= 76.49)
            {
                return 3.00;
            }
            else if (GRADE <= 74.49)
            {
                return 5.00;
            }
            return 0.00;
        }
        private void submitBtn_Click(object sender, EventArgs e)
        {
            CalculateAndDisplayOutput();
        }    
        private void subjectsCb_SelectedIndexChanged(object sender, EventArgs e)
        {
            count = subjectsCb.SelectedIndex + 1;
            statusLbl.Text = $"{count} out of {subjects_list.Count}";
            button1.Enabled = count != 1;
            button2.Enabled = count != subjects_list.Count;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            subjectsCb.SelectedIndex -= 1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            subjectsCb.SelectedIndex += 1;
        }
        private void ValidateInput(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (< (char)48 or > (char)57) and not (char)8 and not (char)46:
                    e.Handled = true;
                    return;
                default:
                    if (e.KeyChar == 46)
                    {
                        if (((TextBox)sender).Text.Contains(e.KeyChar))
                            e.Handled = true;
                    }
                    break;
            }
        }
        private void prelimsTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateInput(sender, e);
        }

        private void midtermsTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateInput(sender, e);
        }

        private void prefinalsTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateInput(sender, e);
        }

        private void finalsTb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ValidateInput(sender, e);
        }
    }
}
