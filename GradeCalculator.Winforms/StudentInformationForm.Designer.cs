﻿#region MIT License
/*
 * Copyright (c) 2024 Glenn Alon
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
#endregion

using System.Drawing;
using System.Windows.Forms;

namespace GradeCalculator.Winforms
{
    partial class StudentInformationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            termCb = new ComboBox();
            label3 = new Label();
            programCb = new ComboBox();
            label2 = new Label();
            levelCb = new ComboBox();
            label1 = new Label();
            label4 = new Label();
            nameTb = new TextBox();
            confirmBtn = new Button();
            clearBtn = new Button();
            SuspendLayout();
            // 
            // termCb
            // 
            termCb.DropDownStyle = ComboBoxStyle.DropDownList;
            termCb.FormattingEnabled = true;
            termCb.Location = new Point(138, 154);
            termCb.Name = "termCb";
            termCb.Size = new Size(190, 23);
            termCb.TabIndex = 17;
            // 
            // label3
            // 
            label3.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label3.Location = new Point(79, 154);
            label3.Name = "label3";
            label3.Size = new Size(47, 23);
            label3.TabIndex = 16;
            label3.Text = "Term:";
            // 
            // programCb
            // 
            programCb.DropDownStyle = ComboBoxStyle.DropDownList;
            programCb.FormattingEnabled = true;
            programCb.Location = new Point(138, 108);
            programCb.Name = "programCb";
            programCb.Size = new Size(190, 23);
            programCb.TabIndex = 15;
            // 
            // label2
            // 
            label2.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label2.Location = new Point(52, 108);
            label2.Name = "label2";
            label2.Size = new Size(80, 23);
            label2.TabIndex = 14;
            label2.Text = "Program:";
            // 
            // levelCb
            // 
            levelCb.DropDownStyle = ComboBoxStyle.DropDownList;
            levelCb.FormattingEnabled = true;
            levelCb.Location = new Point(138, 131);
            levelCb.Name = "levelCb";
            levelCb.Size = new Size(190, 23);
            levelCb.TabIndex = 13;
            // 
            // label1
            // 
            label1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label1.Location = new Point(32, 131);
            label1.Name = "label1";
            label1.Size = new Size(100, 23);
            label1.TabIndex = 12;
            label1.Text = "Level / Year:";
            // 
            // label4
            // 
            label4.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point, 0);
            label4.Location = new Point(70, 85);
            label4.Name = "label4";
            label4.Size = new Size(56, 23);
            label4.TabIndex = 18;
            label4.Text = "Name:";
            // 
            // nameTb
            // 
            nameTb.Location = new Point(138, 85);
            nameTb.Name = "nameTb";
            nameTb.Size = new Size(190, 23);
            nameTb.TabIndex = 19;
            // 
            // confirmBtn
            // 
            confirmBtn.BackColor = Color.SpringGreen;
            confirmBtn.FlatStyle = FlatStyle.Flat;
            confirmBtn.Location = new Point(138, 203);
            confirmBtn.Name = "confirmBtn";
            confirmBtn.Size = new Size(87, 43);
            confirmBtn.TabIndex = 20;
            confirmBtn.Text = "Confirm";
            confirmBtn.UseVisualStyleBackColor = false;
            confirmBtn.Click += confirmBtn_Click;
            // 
            // clearBtn
            // 
            clearBtn.BackColor = Color.IndianRed;
            clearBtn.FlatStyle = FlatStyle.Flat;
            clearBtn.Location = new Point(241, 203);
            clearBtn.Name = "clearBtn";
            clearBtn.Size = new Size(87, 43);
            clearBtn.TabIndex = 21;
            clearBtn.Text = "Clear";
            clearBtn.UseVisualStyleBackColor = false;
            // 
            // StudentInformationForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(410, 303);
            Controls.Add(clearBtn);
            Controls.Add(confirmBtn);
            Controls.Add(nameTb);
            Controls.Add(label4);
            Controls.Add(termCb);
            Controls.Add(label3);
            Controls.Add(programCb);
            Controls.Add(label2);
            Controls.Add(levelCb);
            Controls.Add(label1);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;
            Name = "StudentInformationForm";
            Text = "Student Information Form";
            Load += StudentInformationForm_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox termCb;
        private Label label3;
        private ComboBox programCb;
        private Label label2;
        private ComboBox levelCb;
        private Label label1;
        private Label label4;
        private TextBox nameTb;
        private Button confirmBtn;
        private Button clearBtn;
    }
}