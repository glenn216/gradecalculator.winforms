﻿#region MIT License
/*
 * Copyright (c) 2024 Glenn Alon
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/
#endregion

using System;
using System.Windows.Forms;
using GradeCalculator.Winforms.Exceptions;

namespace GradeCalculator.Winforms
{
    public partial class StudentInformationForm : Form
    {
        private struct StudentInformation
        {
            internal readonly string[] Programs;
            internal readonly string[] Levels;
            internal readonly string[] Terms;

            public StudentInformation()
            {
                Programs = ["BSCS", "BSIT"];
                Levels = ["1st Year", "2nd Year", "3rd Year", "4th Year"];
                Terms = ["1st Term", "2nd Term"];
            }
        }
        private StudentInformation studentInformation;
        public StudentInformationForm()
        {
            InitializeComponent();
            studentInformation = new StudentInformation();
        }

        private void StudentInformationForm_Load(object sender, EventArgs e)
        {
            foreach (string program in studentInformation.Programs)
            {
                programCb.Items.Add(program);
            }
            foreach (string level in studentInformation.Levels)
            {
                levelCb.Items.Add(level);
            }
            foreach (string term in studentInformation.Terms)
            {
                termCb.Items.Add(term);
            }
        }
        private void confirmBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(nameTb.Text) || programCb.SelectedIndex == -1 || levelCb.SelectedIndex == -1 || termCb.SelectedIndex == -1)
                {
                    throw new EmptyFieldsException("ERROR: Required fields cannot be empty. Please try again!");
                }
                else
                {
                    string name = nameTb.Text;
                    int program = programCb.SelectedIndex;
                    int level = levelCb.SelectedIndex;
                    int term = termCb.SelectedIndex;
                    GradeCalculatorForm gradeCalculatorForm = new GradeCalculatorForm(name, program, level, term);
                    Hide();
                    gradeCalculatorForm.Show();
                }
            }
            catch (EmptyFieldsException) { }
            finally
            {
                nameTb.Clear();
                programCb.SelectedIndex = -1;
                levelCb.SelectedIndex = -1;
                termCb.SelectedIndex = -1;
            }
        }
    }
}
